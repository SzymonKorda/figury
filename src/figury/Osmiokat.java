package figury;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Path2D;

public class Osmiokat extends Figura
{

	public Osmiokat(Graphics2D buf, int del, int w, int h) {
		super(buf, del, w, h);
		Path2D myPath = new Path2D.Double();
		myPath.moveTo(0, 0);
		myPath.lineTo(0, 10);
		myPath.lineTo(10, 20);
		myPath.lineTo(20, 20);
		myPath.lineTo(30, 10);
		myPath.lineTo(30, 0);
		myPath.lineTo(20, -10);
		myPath.lineTo(10, -10);
		myPath.lineTo(0, 0);
		shape = myPath;
		aft = new AffineTransform();
		area = new Area(shape);
		
		// TODO Auto-generated constructor stub
	}
	
}
