package figury;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Path2D;

public class Trojkat extends Figura
{

	public Trojkat(Graphics2D buf, int del, int w, int h) { 
		super(buf, del, w, h);
		Path2D myPath = new Path2D.Double();
		
	    
	    myPath.moveTo(0, 0);
	    myPath.lineTo(5, 5*Math.sqrt(5));
	    myPath.lineTo(10, 0);
	    myPath.lineTo(0, 0);
	    
	    shape = myPath;
	    aft = new AffineTransform();
		area = new Area(shape);
		// TODO Auto-generated constructor stub
	}
	

}
