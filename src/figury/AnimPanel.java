package figury;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.Timer;

public class AnimPanel extends JPanel implements ActionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L; 

	// bufor
	Image image;
	// wykreslacz ekranowy
	Graphics2D device;
	// wykreslacz bufora
	Graphics2D buffer;

	private int delay = 40;

	public static Timer timer;

	private static int numer = 0;

	public AnimPanel() {
		super();
		setBackground(Color.WHITE);
		timer = new Timer(delay, this);
	}

	public void initialize() {
		int width = getWidth();
		int height = getHeight();

		image = createImage(width, height);
		buffer = (Graphics2D) image.getGraphics();
		buffer.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		device = (Graphics2D) getGraphics();
		device.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		buffer.setBackground(Color.WHITE);  //biale tlo animacji

	}
	
public void initializeAgain() {  //skalowanie okna
	
		
		image = createImage(getWidth(), getHeight());
		buffer = (Graphics2D) image.getGraphics();
		buffer.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		device = (Graphics2D) getGraphics();
		device.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		buffer.setBackground(Color.WHITE);
		
		Figura.newShape(getWidth(), getHeight(), buffer); 
	
		
	}

	void addFig() {
		
		Figura fig = null;
		int nrfig = numer++ % 6;
		if(nrfig == 0)  //dodawanie figury 
		{
			fig = new Kwadrat(buffer, delay, getWidth(), getHeight());
		}
		else if (nrfig == 1)
		{
			fig = new Elipsa(buffer, delay, getWidth(), getHeight());
		}
		else if (nrfig == 2)
		{
			fig = new Kolo(buffer, delay, getWidth(), getHeight());
		}
		else if (nrfig == 3)
		{
			fig = new Prostokat(buffer, delay, getWidth(), getHeight());
		}
		else if (nrfig == 4)
		{
			fig = new Trojkat(buffer, delay, getWidth(), getHeight());
		} 
		else if (nrfig == 5)
		{
			fig = new Osmiokat(buffer, delay, getWidth(), getHeight());
		}
		timer.addActionListener(fig);
		new Thread(fig).start();
	}

	void animate() {
		if (timer.isRunning()) {
			timer.stop();
		} else {
			timer.start();
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		device.drawImage(image, 0, 0, null);
		buffer.clearRect(0, 0, getWidth(), getHeight());
	}
}
